/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
    if (typeof window.CustomEvent === 'function') { return false; }

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {

    var tl;
    var win = window;

    function doClickTag() { window.open(window.clickTag); }

    function initTimeline() {
        document.querySelector('#ad .banner').style.display = 'block';
        document.getElementById('ad').addEventListener('click', doClickTag);
        createTimeline();
    }

    function createTimeline() {
        tl = new TimelineMax({delay: 0.25, onStart: updateStart, onComplete: updateComplete, onUpdate: updateStats});
        // ---------------------------------------------------------------------------

        tl.add('frame1')
        .from('.hl1', 1.75, {opacity: 0, ease: Power3.easeOut }, 'frame1')
        .to("#ribbonmask", .75, {attr:{x:0}, ease: Expo.easeOut}, 'frame1+=1.1')
        .from('.hl2', 1, {opacity: 0, ease: Power3.easeOut }, 'frame1+=2')
        .from('.logo2', 1, {opacity: 0, ease: Power3.easeOut }, 'frame1+=3')
        .to('.hl1', 1.25, {opacity: 0, ease: Power3.easeOut }, 'frame1+=4.25')
        .to('#ribbonmask', 1.25, {opacity: 0, ease: Power3.easeOut }, 'frame1+=4.25')
        .to('.hl2', 1.25, {opacity: 0, ease: Power3.easeOut }, 'frame1+=4.25')
        .to('.logo2', 1.25, {opacity: 0, ease: Power3.easeOut, }, 'frame1+=4.25')
        .from('.bo', 1, {opacity: 0, ease: Power3.easeOut }, 'frame1+=5.25')
        .from('.fidel', 1, {opacity: 0, ease: Power3.easeOut }, 'frame1+=6')
        .from('.cta', 1, {opacity: 0, ease: Power3.easeOut }, 'frame1+=6.5')
        .from('.fidc', 1, {opacity: 0, ease: Power3.easeOut }, 'frame1+=6.5')
  



        // ---------------------------------------------------------------------------

        // DEBUG:
        // tl.play('frame3'); // start playing at label:frame3
        // tl.pause('frame3'); // pause the timeline at label:frame3
    }

    function updateStart() {
        var start = new CustomEvent('start', {
            'detail': { 'hasStarted': true }
        });
        win.dispatchEvent(start);
    }

    function updateComplete() {
        var complete = new CustomEvent('complete', {
            'detail': { 'hasStopped': true }
        });
        win.dispatchEvent(complete);
    }

    function updateStats() {
        var statistics = new CustomEvent('stats', {
            'detail': { 'totalTime': tl.totalTime(), 'totalProgress': tl.totalProgress(), 'totalDuration': tl.totalDuration()
            }
        });
        win.dispatchEvent(statistics);
    }

    function getTimeline() {
        return tl;
    }

    return {
        init: initTimeline,
        get: getTimeline
    };

})();

// Banner Init
// ====================================================================================================
timeline.init();
